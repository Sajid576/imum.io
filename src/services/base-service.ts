import * as cheerio from 'cheerio';
import * as https from 'https';
import { Item } from '../models/item';

export const getHtml = async (
  hostname: string,
  path: string
): Promise<string> =>
  new Promise((resolve, reject) => {
    https
      .get(
        {
          hostname,
          path,
          method: 'GET',
        },
        (res) => {
          let html = '';
          res.on('data', function (chunk) {
            html += chunk;
          });
          res.on('end', function () {
            resolve(html);
          });
        }
      )
      .on('error', (error) => {
        console.error(error);
        reject(error);
      });
  });

export const getTotalPages = (pattern: string, html: string) => {
  const $ = cheerio.load(html);

  const temp = $(pattern)
    .map((index, element: cheerio.Element) => {
      const pageNumber = $(element).find('span').text().trim();
      return {
        pageNumber,
      };
    })
    .get();

  return parseInt(temp[temp.length - 1].pageNumber);
};

export const scrapeSingleListPage = async (
  pattern: string,
  subpatterns: any[],
  html: string
): Promise<Item[]> => {
  const $ = cheerio.load(html);

  const ads = $(pattern)
    .map((index, element: cheerio.Element) => {
      const temp: any = {};

      for (let subpattern of subpatterns) {
        if (subpattern.type === 'text') {
          temp[subpattern.name] = $(element)
            .find(subpattern.filterBy)
            .text()
            .trim();
        } else if (subpattern.type === 'attr') {
          temp[subpattern.name] = subpattern.filterBy
            ? $(element).find(subpattern.filterBy).attr(subpattern.attrName) ||
              ''
            : $(element).attr(subpattern.attrName);
        }
      }

      return { id: index + 1, ...temp };
    })
    .get();

  return ads;
};

export const scrapeTruckItem = async (
  pattern: string,
  targetTruckDetailKeys: any,
  baseUrl: string,
  item: Item
) => {
  const url = item.href;
  let html = await getHtml(baseUrl, url.replace('https://' + baseUrl, ''));

  const $ = cheerio.load(html);

  const truckDetails: any = {};

  //'main div.parametersArea div.offer-params.with-vin ul.offer-params__list li.offer-params__item';

  $(pattern)
    .map((_, element: cheerio.Element) => {
      const key = $(element).find('span').text().trim();
      const value = $(element).find('div').text().trim();

      const outKey = getValidKey(key, targetTruckDetailKeys);

      if (outKey) {
        truckDetails[outKey] = value;
      }
    })
    .get();

  return truckDetails;
};

export const getValidKey = (
  inputKey: string,
  targetTruckDetailKeys: any
): string | undefined => {
  for (let key in targetTruckDetailKeys) {
    if (targetTruckDetailKeys[key].includes(inputKey)) {
      return key;
    }
  }
};
